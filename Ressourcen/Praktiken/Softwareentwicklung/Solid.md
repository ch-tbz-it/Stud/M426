# SOLID

[TOC]

## Single responsibility principle [SRP]
Das Single Responsibility Principle (SRP) besagt, dass eine Klasse nur einen Grund zur Änderung haben sollte. Mit anderen Worten sollte eine Klasse nur eine Verantwortlichkeit oder Aufgabe haben. Wenn eine Klasse mehr als eine Verantwortlichkeit hat, wird sie anfälliger für Änderungen und schwerer zu warten.

Hier ist ein einfaches Beispiel, um das SRP zu erklären. Betrachten Sie eine Klasse, die für die Verwaltung von Mitarbeitern in einem Unternehmen zuständig ist:

```java
// Eine Klasse, die mehrere Verantwortlichkeiten hat
class EmployeeManager {
    public void addEmployee(Employee employee) {
        // Logik zum Hinzufügen eines Mitarbeiters
    }

    public void calculatePay(Employee employee) {
        // Logik zur Berechnung des Gehalts
    }

    public void generateReport(Employee employee) {
        // Logik zur Erstellung eines Berichts
    }
}
```

In diesem Beispiel hat die Klasse `EmployeeManager` mehrere Verantwortlichkeiten:

1. `addEmployee`: Hinzufügen eines Mitarbeiters.
2. `calculatePay`: Berechnung des Gehalts.
3. `generateReport`: Erstellung eines Berichts.

Das Problem hierbei ist, dass jede dieser Funktionen einen anderen Grund zur Änderung haben kann. Wenn sich die Logik für die Gehaltsberechnung ändert, könnte dies Auswirkungen auf die Klasse haben, auch wenn der Rest unverändert bleibt. Das verstößt gegen das SRP.

Um das SRP einzuhalten, könnten Sie die Verantwortlichkeiten auf verschiedene Klassen aufteilen:

```java
// Eine Klasse für die Verwaltung von Mitarbeitern
class EmployeeManager {
    public void addEmployee(Employee employee) {
        // Logik zum Hinzufügen eines Mitarbeiters
    }
}

// Eine separate Klasse für die Gehaltsberechnung
class PayrollCalculator {
    public void calculatePay(Employee employee) {
        // Logik zur Berechnung des Gehalts
    }
}

// Eine separate Klasse für die Berichterstellung
class ReportGenerator {
    public void generateReport(Employee employee) {
        // Logik zur Erstellung eines Berichts
    }
}
```

Durch diese Aufteilung hat jede Klasse jetzt nur eine Verantwortlichkeit. Änderungen an der Gehaltsberechnung beeinflussen nicht die Klasse für das Hinzufügen von Mitarbeitern oder die Klasse für die Berichterstellung. Dies macht den Code robuster und wartungsfreundlicher.

## Open closed principle [OCP]
Das Open/Closed Principle (OCP) besagt, dass eine Klasse für Erweiterungen offen sein sollte, aber für Änderungen geschlossen. Mit anderen Worten sollte der Code so gestaltet sein, dass Sie neue Funktionen hinzufügen können, ohne den bestehenden Code zu ändern.

Hier ist ein einfaches Beispiel, um das OCP zu erklären. Angenommen, Sie haben eine Klasse `Shape` und möchten verschiedene Arten von Formen berechnen und zeichnen:

```java
// Grundlegende Klasse für Formen
class Shape {
    public String type;

    public Shape(String type) {
        this.type = type;
    }
}

// Klasse für die Berechnung von Flächen
class AreaCalculator {
    public double calculateArea(Shape shape) {
        if (shape.type.equals("Circle")) {
            // Logik zur Berechnung der Kreisfläche
            return Math.PI * Math.pow(10, 2); // Beispielwert für den Radius 10
        } else if (shape.type.equals("Rectangle")) {
            // Logik zur Berechnung der Rechteckfläche
            return 5 * 10; // Beispielwerte für die Seitenlängen 5 und 10
        }
        return 0;
    }
}
```

Im obigen Beispiel verstößt die `AreaCalculator`-Klasse gegen das OCP, da Sie jedes Mal, wenn eine neue Form hinzugefügt wird, den Code dieser Klasse ändern müssten, um die Fläche dieser neuen Form zu berechnen.

Um das OCP einzuhalten, könnten Sie eine Erweiterung vornehmen, indem Sie eine abstrakte Klasse oder ein Interface für die Berechnung von Flächen erstellen und dann für jede Form eine eigene Implementierung bereitstellen:

```java
// Abstrakte Klasse oder Interface für die Berechnung von Flächen
interface AreaCalculator {
    double calculateArea();
}

// Kreis-Implementierung
class CircleAreaCalculator implements AreaCalculator {
    @Override
    public double calculateArea() {
        // Logik zur Berechnung der Kreisfläche
        return Math.PI * Math.pow(10, 2); // Beispielwert für den Radius 10
    }
}

// Rechteck-Implementierung
class RectangleAreaCalculator implements AreaCalculator {
    @Override
    public double calculateArea() {
        // Logik zur Berechnung der Rechteckfläche
        return 5 * 10; // Beispielwerte für die Seitenlängen 5 und 10
    }
}
```

Durch diese Struktur können Sie neue Formen hinzufügen, indem Sie einfach eine neue Implementierung des `AreaCalculator` erstellen, ohne den bestehenden Code zu ändern. Dadurch bleibt die `AreaCalculator` für Erweiterungen offen und für Änderungen geschlossen, was das OCP demonstriert.

## Liskov Substitution Principle [LSP]
Das Liskov Substitution Principle (LSP) ist eines der SOLID-Prinzipien in der objektorientierten Programmierung und besagt, dass Objekte einer Basisklasse durch Objekte einer abgeleiteten Klasse substituiert werden können, ohne das erwartete Verhalten zu ändern.

Betrachten wir eine Klasse `Rectangle`, die die Breite und Höhe eines Rechtecks repräsentiert:

```java
class Rectangle {
    protected int width;
    protected int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int calculateArea() {
        return width * height;
    }
}
```

Nun könnte jemand auf die Idee kommen, eine spezialisiertere Klasse namens `Square` zu erstellen, die von `Rectangle` erbt. Ein Quadrat ist natürlich nur ein spezieller Fall eines Rechtecks, bei dem die Breite und Höhe gleich sind:

```java
class Square extends Rectangle {
    public Square(int side) {
        super(side, side);
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
        super.setHeight(width);
    }

    @Override
    public void setHeight(int height) {
        super.setHeight(height);
        super.setWidth(height);
    }
}
```

Auf den ersten Blick scheint dies vernünftig zu sein. Ein Quadrat ist einfach ein spezielles Rechteck, oder? Nun, hier liegt das Problem, wenn wir das Liskov Substitution Principle (LSP) nicht beachten.

Schauen wir uns einen Codeabschnitt an:

```java
public static void resizeRectangle(Rectangle rectangle, int newWidth, int newHeight) {
    rectangle.setWidth(newWidth);
    rectangle.setHeight(newHeight);
}

public static void main(String[] args) {
    Rectangle rectangle = new Rectangle(2, 3);
    resizeRectangle(rectangle, 5, 10);

    System.out.println("Area after resizing: " + rectangle.calculateArea());  // Überraschung!
    
    Square square = new Square(5);
    resizeRectangle(square, 4, 8);

    System.out.println("Area of the square: " + square.calculateArea());  // Unerwartetes Ergebnis!
}
```

Nach der Vergrößerung des Rechtecks und des Quadrats ist das Ergebnis für die Fläche des Quadrats nicht wie erwartet. Dies liegt daran, dass das `Square` die Erwartungen an das Verhalten eines Rechtecks verletzt, wenn es die Breite oder Höhe ändert. Das LSP besagt, dass ein Subtyp (hier `Square`) genauso verwendet werden können sollte wie sein Basistyp (`Rectangle`), ohne dass dies zu unerwartetem Verhalten führt. In diesem Fall verletzt das `Square`-Beispiel das LSP, weil es sich anders verhält als ein typisches `Rectangle`.

> Um das Liskov Substitution Principle (LSP) einzuhalten, müsste die Hierarchie der Klassen so gestaltet werden, dass die spezialisierte Klasse (`Square`) keine zusätzlichen Bedingungen oder Restriktionen auferlegt, die über diejenigen der Basisklasse (`Rectangle`) hinausgehen. Insbesondere sollten die Methoden in der spezialisierten Klasse konsistent mit den Erwartungen für die Basisklasse arbeiten.

Im vorliegenden Beispiel könnten Sie die Hierarchie überdenken und eine andere Struktur wählen. Ein Ansatz wäre, eine gemeinsame Schnittstelle für alle Vierecke (Rechtecke und Quadrate) zu definieren und sicherzustellen, dass die spezialisierten Klassen diese Schnittstelle einheitlich implementieren.

Hier ist eine mögliche Lösung:

```java
// Gemeinsame Schnittstelle für alle Vierecke
interface Quadrilateral {
    int getWidth();
    void setWidth(int width);
    int getHeight();
    void setHeight(int height);
    int calculateArea();
}

// Rectangle implementiert die Schnittstelle
class Rectangle implements Quadrilateral {
    protected int width;
    protected int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    // Implementierung der Schnittstelle
    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int calculateArea() {
        return width * height;
    }
}

// Square implementiert ebenfalls die Schnittstelle
class Square implements Quadrilateral {
    private int side;

    public Square(int side) {
        this.side = side;
    }

    // Implementierung der Schnittstelle
    @Override
    public int getWidth() {
        return side;
    }

    @Override
    public void setWidth(int width) {
        this.side = width;
    }

    @Override
    public int getHeight() {
        return side;
    }

    @Override
    public void setHeight(int height) {
        this.side = height;
    }

    @Override
    public int calculateArea() {
        return side * side;
    }
}
```

Mit dieser Struktur können Sie sicherstellen, dass sowohl `Rectangle` als auch `Square` die gleiche Schnittstelle (`Quadrilateral`) implementieren, was bedeutet, dass sie konsistent verwendet werden können, ohne unerwartetes Verhalten zu erzeugen.

## Interface segregation principle [ISP]
Das Interface Segregation Principle (ISP) besagt, dass eine Klasse nicht gezwungen sein sollte, Methoden zu implementieren, die sie nicht verwendet. Wenn eine Klasse ein Interface implementiert, sollte sie nur für diejenigen Methoden verantwortlich sein, die für ihre Funktionalität relevant sind.

Hier ist ein einfaches Beispiel, um das ISP zu erklären. Angenommen, Sie haben ein Interface `Worker`:

```java
// Interface für Arbeiter
interface Worker {
    void work();
    void eat();
}
```

Sie haben zwei Klassen, `Robot` und `Human`, die dieses Interface implementieren:

```java
class Robot implements Worker {
    @Override
    public void work() {
        // Logik für die Arbeit des Roboters
    }

    @Override
    public void eat() {
        // Roboter essen nicht, daher leer
    }
}

class Human implements Worker {
    @Override
    public void work() {
        // Logik für die Arbeit des Menschen
    }

    @Override
    public void eat() {
        // Logik für das Essen des Menschen
    }
}
```

In diesem Beispiel verstößt die Klasse `Robot` gegen das ISP, da Roboter normalerweise nicht essen, aber dennoch gezwungen sind, die `eat`-Methode zu implementieren. Das führt zu einer unnötigen Implementierung und möglicherweise zu leeren Methoden, die keine wirkliche Funktion haben.

Um das ISP einzuhalten, könnten Sie das Interface in kleinere, spezifischere Teile aufteilen:

```java
// Interface für die Arbeit
interface Workable {
    void work();
}

// Interface für das Essen
interface Eatable {
    void eat();
}

// Klasse Robot implementiert nur Workable
class Robot implements Workable {
    @Override
    public void work() {
        // Logik für die Arbeit des Roboters
    }
}

// Klasse Human implementiert sowohl Workable als auch Eatable
class Human implements Workable, Eatable {
    @Override
    public void work() {
        // Logik für die Arbeit des Menschen
    }

    @Override
    public void eat() {
        // Logik für das Essen des Menschen
    }
}
```

Durch diese Aufteilung können Klassen nur die Teile des Interfaces implementieren, die für ihre spezifische Funktionalität relevant sind, und nicht gezwungen werden, unnötige Methoden zu implementieren. Das ist eine Anwendung des Interface Segregation Principle.

## Dipendency inversion principle [DIP]
Das Dependency Inversion Principle (DIP) besagt, dass Hochrangige Module nicht von Niedrigrangigen abhängen sollten, sondern beide von abstrakten Abstractions abhängen sollten. Mit anderen Worten: Abhängigkeiten sollten von abstrakten Schnittstellen oder Klassen abgeleitet werden, nicht von konkreten Implementierungen.

Hier ist ein einfaches Beispiel, um das DIP zu erklären. Betrachten Sie eine Anwendung, die Benachrichtigungen sendet. Zunächst könnten Sie eine konkrete Implementierung für das Versenden von E-Mails haben:

```java
// Konkrete Implementierung für das Versenden von E-Mails
class EmailSender {
    public void sendEmail(String to, String message) {
        // Logik für das Versenden von E-Mails
        System.out.println("Email sent to " + to + ": " + message);
    }
}
```

Dann haben Sie eine High-Level-Klasse (`NotificationService`), die von dieser konkreten Implementierung abhängt:

```java
// Hochrangiges Modul, das von einer konkreten Implementierung abhängt
class NotificationService {
    private EmailSender emailSender = new EmailSender();

    public void sendNotification(String recipient, String message) {
        // Verwendung der konkreten Implementierung
        emailSender.sendEmail(recipient, message);
    }
}
```

In diesem Beispiel verletzt die Klasse `NotificationService` das Dependency Inversion Principle, da sie direkt von einer konkreten Implementierung (`EmailSender`) abhängt. Um das Prinzip zu respektieren, würden Sie stattdessen von einer abstrakten Abstraktion abhängen:

```java
// Abstrakte Schnittstelle für das Senden von Benachrichtigungen
interface MessageSender {
    void sendMessage(String to, String message);
}

// Konkrete Implementierung für das Versenden von E-Mails
class EmailSender implements MessageSender {
    @Override
    public void sendMessage(String to, String message) {
        // Logik für das Versenden von E-Mails
        System.out.println("Email sent to " + to + ": " + message);
    }
}
```

Die High-Level-Klasse (`NotificationService`) würde dann von der abstrakten Schnittstelle abhängen:

```java
// Hochrangiges Modul, das von einer abstrakten Schnittstelle abhängt
class NotificationService {
    private MessageSender messageSender;

    // Konstruktor für Dependency Injection
    public NotificationService(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public void sendNotification(String recipient, String message) {
        // Verwendung der abstrakten Schnittstelle
        messageSender.sendMessage(recipient, message);
    }
}
```

Durch die Anwendung des Dependency Inversion Principle wird die `NotificationService`-Klasse nicht mehr von einer konkreten Implementierung abhängig, sondern von einer abstrakten Schnittstelle (`MessageSender`), was die Flexibilität und Wartbarkeit der Anwendung verbessert.

---
**Quellen**<br/>
https://www.educative.io/answers/what-are-the-solid-principles-in-java <br/>
https://www.educative.io/blog/solid-principles-oop-c-sharp

