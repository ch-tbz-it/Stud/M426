# Scrum

## [Produktevision](./Produktevision/)

## [Scrum Flow](./Scrum%20flow/Readme.md)

## [Rollen](./Rollen/)

## [Zeremonien](./Zeremonien/)

## [Backlog/ Epics/ User Stories](./Backlog_epic_userstory/Readme.md)

## [Definition of Done](./Definition_of_Done/Readme.md)

## [Messungen](./Messungen/)

## [Werte und Prinzipien](./Werte_und_Prinzipien/Readme.md)

## [Agile triangle/ Stacy Matrix](./Agile%20Triangle%20Stacy%20Matrix/)
