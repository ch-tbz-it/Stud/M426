# Scrum Flow

[Hier](https://www.youtube.com/watch?v=ZiEcq9uvi4Y&t=1s) wird in Kürze der Scrum Flow auf eine einfache Art und Weise erklärt.

Detaillierte Erklärungen sind in [scrum guide](https://www.scrumguides.org/scrum-guide.html) zu finden.

Dieser [Poster](./Poster/Scrum%20Guide%20Poster%20de%202.0.pdf) verschafft einen Überblick über die Scrum Rollen, Zeremonien und Artefakte.