# SCRUM Werte

Scrum ist ein agiles Framework für die Softwareentwicklung, das auf fünf zentralen Werten basiert, die die Zusammenarbeit, Kommunikation und den Erfolg von Teams fördern. Diese fünf Werte sind:

1. Transparenz (Transparency): In Scrum wird Wert auf Transparenz in allen Aspekten des Projekts gelegt. Das bedeutet, dass alle relevanten Informationen für alle Teammitglieder und Stakeholder leicht zugänglich und verständlich sein sollten. Dies umfasst die Transparenz von Arbeitsfortschritten, Hindernissen, Anforderungen und Risiken. Transparenz ermöglicht es, ein gemeinsames Verständnis zu schaffen und informierte Entscheidungen zu treffen.

2. Inspektion (Inspection): In regelmäßigen Abständen überprüft das Scrum-Team den Fortschritt seiner Arbeit und die Ergebnisse. Diese Inspektionen erfolgen in Sprint-Reviews, Daily Standups und Retrospektiven. Durch die kontinuierliche Inspektion können Abweichungen von den Zielen und Qualitätsstandards frühzeitig erkannt werden, was die Möglichkeit zur Anpassung und Verbesserung bietet.

3. Anpassung (Adaptation): Auf der Grundlage der Inspektionsergebnisse und des Feedbacks passt das Scrum-Team seine Arbeitsweise und Prioritäten kontinuierlich an. Dies bedeutet, dass Teams bereit sein sollten, auf Veränderungen zu reagieren und ihren Ansatz zu optimieren, um bessere Ergebnisse zu erzielen. Die Anpassung ist ein wesentlicher Teil des agilen Denkens.

4. Mut (Courage): Mut ist erforderlich, um unangenehme Wahrheiten anzusprechen, Risiken zu identifizieren und Hindernisse zu überwinden. In Scrum ermutigt man die Teammitglieder und Stakeholder, mutig zu sein und Schwierigkeiten anzugehen, anstatt sie zu verbergen oder zu umgehen. Mut ist wichtig, um kontinuierliche Verbesserung zu fördern.

5. Respekt (Respect): Respekt für die Fähigkeiten, Meinungen und Perspektiven jedes Teammitglieds ist ein zentraler Wert in Scrum. Dies schafft eine unterstützende und kollaborative Umgebung, in der Menschen ihr Bestes geben können. Respekt hilft auch bei der Lösung von Konflikten und fördert die Zusammenarbeit im Team.

Diese fünf Werte sind grundlegend für die Umsetzung von Scrum und die Schaffung einer agilen Kultur, in der Teams erfolgreich arbeiten können. Sie sind eng miteinander verknüpft und bilden die Grundlage für die agilen Praktiken und Prinzipien, die in Scrum angewendet werden.

---
Quellen: <br/>
- https://chaosverbesserer.de/blog/2017/02/12/die-5-werte-von-scrum/