# Agile Manifesto
Das Agile Manifesto ist ein wegweisendes Dokument, das 2001 von einer Gruppe von Softwareentwicklern erstellt wurde, die verschiedene agile Methoden praktizierten. Das Manifest fasst die Prinzipien und Werte zusammen, die die Grundlage für agile Softwareentwicklung bilden. Es besteht aus vier grundlegenden Werten und zwölf Prinzipien, die die agilen Praktiken und Ansätze für die Softwareentwicklung definieren. Hier ist eine Erklärung des Agile Manifesto:

**Vier Werte des Agile Manifesto:**

1. **Individuen und Interaktionen über Prozesse und Werkzeuge (Individuals and Interactions over Processes and Tools):** Dieser Wert betont die Bedeutung von Menschen und ihren Beziehungen in der Softwareentwicklung. Agile Methoden legen Wert darauf, dass Kommunikation und Zusammenarbeit innerhalb des Teams und mit den Stakeholdern Priorität haben.

2. **Funktionierende Software über umfassende Dokumentation (Working Software over Comprehensive Documentation):** Agile Entwicklung priorisiert die Lieferung funktionsfähiger Software gegenüber umfangreicher Dokumentation. Dokumentation ist wichtig, aber sie sollte pragmatisch sein und den Entwicklungsprozess nicht verlangsamen.

3. **Zusammenarbeit mit dem Kunden über Vertragsverhandlungen (Customer Collaboration over Contract Negotiation):** Dieser Wert betont die Zusammenarbeit mit Kunden und Benutzern, um ihre Anforderungen und Erwartungen zu verstehen und flexibel auf Änderungen zu reagieren, anstatt sich auf starre Verträge zu verlassen.

4. **Reagieren auf Veränderung über das Befolgen eines Plans (Responding to Change over Following a Plan):** Agile Entwicklung erkennt an, dass sich Anforderungen und Umstände im Laufe eines Projekts ändern können. Anstatt stur an einem ursprünglichen Plan festzuhalten, sollten agile Teams bereit sein, sich anzupassen und auf Veränderungen zu reagieren.

**Zwölf Prinzipien des Agile Manifesto:**

Die [zwölf Prinzipien des Agile Manifesto](https://businessmap.io/de/agile-de/projektmanagement/prinzipien) erweitern und konkretisieren die vier Werte. Sie bieten eine Anleitung für die Umsetzung agiler Praktiken und sind in erster Linie darauf ausgerichtet, die Kundenzufriedenheit und die Effektivität der Softwareentwicklung zu verbessern.

Die agile Softwareentwicklung basiert auf der Annahme, dass die Zusammenarbeit zwischen individuellen Entwicklern und Benutzern sowie die Bereitschaft zur Veränderung und kontinuierlichen Verbesserung dazu beitragen, bessere Produkte schneller zu liefern. Das Agile Manifesto ist zu einem wichtigen Leitfaden für agile Teams und Organisationen geworden, die flexiblere und kundenorientiertere Ansätze in der Softwareentwicklung verfolgen möchten.

---
Quellen: <br/>
- https://agilemanifesto.org/
- https://businessmap.io/de/agile-de/projektmanagement/prinzipien
- https://digitaleneuordnung.de/blog/agile-werte/
