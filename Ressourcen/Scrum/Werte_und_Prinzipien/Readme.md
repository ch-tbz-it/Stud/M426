# Agilen Werte und Prinzipien
Agile Werte und Prinzipien bilden die Grundhaltung für agile Praktiken und Methoden. Ob diese Werte und Prinzipien tatsächlich gelebt werden, hängt von jedem Einzelnen ab, da es eine bewusste Entscheidung ist, sie in das eigene Verhalten zu integrieren.

[Agile Vorgehensweise - Agile Manifesto - Agilen Werte und Prinzipien](Agile%20Vorgehensweise.md)


Quellen: <br/>
- https://digitaleneuordnung.de/blog/agile-werte/
- [Agile Manifesto](./AgileManifesto.md)
- [SCRUM Werte](./Scrum%20Werte.md)
- [Agile Werte und Prinzipien](./Agile%20Werte%20und%20Prinzipien.pdf)