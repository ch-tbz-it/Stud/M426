[TOC]
# Agile Vorgehensweise - Agile Manifesto - Werte und Prinzipien

Das [Agile Manifesto](https://agilemanifesto.org) wurde im Februar 2001 von einer Gruppe von Softwareentwicklern veröffentlicht und bildet die grundlegende Basis für die agile Softwareentwicklung. Dieses Manifest legte die agilen Werte und Prinzipien fest, die die Grundlage für agile Arbeitsweisen in der Softwareentwicklung bilden.

Das SCRUM-Framework und die [SCRUM Werte](https://www.scrum.org/resources/scrum-values) bauen auf den allgemeinen agilen Werten und Prinzipien auf, welche im Agile Manifesto definiert sind. In Scrum konzentriert man sich auf die Umsetzung dieser agilen Werte und Prinzipien in den Rahmen des Scrum-Frameworks.

Das Agile Manifesto und die agilen Werte sind also die grundlegenden Konzepte, während Scrum und andere agile Frameworks praktische Ansätze zur Umsetzung dieser Konzepte bieten. **Beide sind entscheidend für den Erfolg agiler Softwareentwicklung.**

## Das Agile Manifesto
Das [Agile Manifesto](https://agilemanifesto.org) ist ein wegweisendes Dokument, das 2001 von einer Gruppe von Softwareentwicklern erstellt wurde, die verschiedene agile Methoden praktizierten. Das Manifest fasst die Prinzipien und Werte zusammen, die die Grundlage für agile Softwareentwicklung bilden. Es besteht aus vier grundlegenden Werten und zwölf Prinzipien, die die agilen Praktiken und Ansätze für die Softwareentwicklung definieren. Hier ist eine Erklärung des Agile Manifesto:

**Vier Werte des Agile Manifesto:**

1. **Individuen und Interaktionen über Prozesse und Werkzeuge (Individuals and Interactions over Processes and Tools):** Dieser Wert betont die Bedeutung von Menschen und ihren Beziehungen in der Softwareentwicklung. Agile Methoden legen Wert darauf, dass Kommunikation und Zusammenarbeit innerhalb des Teams und mit den Stakeholdern Priorität haben.

2. **Funktionierende Software über umfassende Dokumentation (Working Software over Comprehensive Documentation):** Agile Entwicklung priorisiert die Lieferung funktionsfähiger Software gegenüber umfangreicher Dokumentation. Dokumentation ist wichtig, aber sie sollte pragmatisch sein und den Entwicklungsprozess nicht verlangsamen.

3. **Zusammenarbeit mit dem Kunden über Vertragsverhandlungen (Customer Collaboration over Contract Negotiation):** Dieser Wert betont die Zusammenarbeit mit Kunden und Benutzern, um ihre Anforderungen und Erwartungen zu verstehen und flexibel auf Änderungen zu reagieren, anstatt sich auf starre Verträge zu verlassen.

4. **Reagieren auf Veränderung über das Befolgen eines Plans (Responding to Change over Following a Plan):** Agile Entwicklung erkennt an, dass sich Anforderungen und Umstände im Laufe eines Projekts ändern können. Anstatt stur an einem ursprünglichen Plan festzuhalten, sollten agile Teams bereit sein, sich anzupassen und auf Veränderungen zu reagieren.

## Zwölf Prinzipien des Agile Manifesto

Die [zwölf Prinzipien des Agile Manifesto](https://businessmap.io/de/agile-de/projektmanagement/prinzipien) erweitern und konkretisieren die vier Werte. Sie bieten eine Anleitung für die Umsetzung agiler Praktiken und sind in erster Linie darauf ausgerichtet, die Kundenzufriedenheit und die Effektivität der Softwareentwicklung zu verbessern.

![](./../../../x_gitressourcen/12werteundprinzipien.png)

Die agile Softwareentwicklung basiert auf der Annahme, dass die Zusammenarbeit zwischen individuellen Entwicklern und Benutzern sowie die Bereitschaft zur Veränderung und kontinuierlichen Verbesserung dazu beitragen, bessere Produkte schneller zu liefern. Das Agile Manifesto ist zu einem wichtigen Leitfaden für agile Teams und Organisationen geworden, die flexiblere und kundenorientiertere Ansätze in der Softwareentwicklung verfolgen möchten.

## Ableitung agiler Methoden
Das Agile Manifesto legt die Grundlagen für flexible, kundenorientierte und teamzentrierte Arbeitsweisen. Die vier zentralen Werte werden in zwölf Prinzipien vertieft. Diese Prinzipien fördern Kundenzufriedenheit durch frühe und regelmäßige Auslieferungen, ständige Kommunikation, Selbstorganisation im Team, nachhaltiges Arbeiten und kontinuierliche Verbesserung.

Zur Umsetzung greifen Teams auf agile Praktiken zurück, die das tägliche Arbeiten strukturieren und verbessern. Das **Daily Scrum**-Meeting dient dazu, den Fortschritt zu besprechen und Hindernisse zu identifizieren, während **kurze Iterationen** regelmäßiges Feedback und schnelle Anpassungen ermöglichen. **Retrospektiven** helfen dem Team, nach jedem Sprint zu reflektieren und die eigene Arbeit zu optimieren. Weitere Praktiken wie **Pair Programming**, **Test-Driven Development (TDD)**, bei dem Tests vor dem Code geschrieben werden, und **Continuous Integration (CI)** fördern hohe Codequalität. Durch regelmäßiges **Backlog Grooming** priorisiert das Team Aufgaben, sodass die Ausrichtung klar bleibt.

![](./../../../x_gitressourcen/ableitungagilermethoden.png)

Zu den agilen Methoden gehören **Scrum** mit festgelegten Rollen und kurzen Sprints, **Kanban** zur Visualisierung und Optimierung des Arbeitsflusses und **Lean Startup** für schnelles Kundenfeedback und gezielte Experimente. **Extreme Programming (XP)** unterstützt mit Techniken wie Pair Programming und TDD die technische Qualität, während **Feature-Driven Development (FDD)** sich auf funktionsorientierte Entwicklung konzentriert. Auch **Crystal** bietet agile Prinzipien, die je nach Projektgröße und Komplexität angepasst werden können.

Teams kombinieren oft verschiedene Methoden und Praktiken, um den agilen Ansatz flexibel an ihre spezifischen Anforderungen anzupassen. So können Scrum und Kanban gemischt werden, um die Struktur von Scrum zu nutzen und gleichzeitig den Arbeitsfluss besser sichtbar zu machen. Techniken aus Extreme Programming können integriert werden, um den Fokus auf die Codequalität zu verstärken. Hybride Ansätze ermöglichen es, flexibel und bedarfsorientiert zu arbeiten, ohne die agilen Werte und Prinzipien zu verlieren.

## Agile Pyramide
Die [Agile Pyramide](https://digitaleneuordnung.de/blog/agile-werte/) veranschaulicht das Zusammenspiel von agilen Werten, Prinzipien, Methoden und Praktiken in zwei Hauptbereichen: **Being Agile** und **Doing Agile**. 

![](./../../../x_gitressourcen/agilepyramide.png)

Im Bereich **Being Agile** bilden die **agilen Werte** die Basis der agilen Kultur. Diese Werte (wie z. B. Flexibilität und Kundenorientierung) bestimmen die Grundhaltung im Team und schaffen eine Kultur, die auf Zusammenarbeit und Anpassungsfähigkeit beruht. Die **agilen Prinzipien** bauen auf diesen Werten auf und geben klare Leitlinien, wie das agile Arbeiten gestaltet werden soll, z. B. durch kontinuierliche Verbesserung und die Förderung von Eigenverantwortung.

Im Bereich **Doing Agile** finden sich die **agilen Methoden und Praktiken**. Diese dienen dazu, die Werte und Prinzipien im Alltag umzusetzen. Agile Methoden wie **Scrum** oder **Kanban** strukturieren die Arbeit, während agile Praktiken wie das **Daily Scrum**-Meeting oder **Retrospektiven** für den operativen Ablauf sorgen.

Durch das Zusammenspiel von **Being Agile** und **Doing Agile** entsteht eine vollständige agile Arbeitsweise: Die Werte und Prinzipien (Being Agile) schaffen die nötige Denkweise, während Methoden und Praktiken (Doing Agile) für die praktische Umsetzung sorgen.

## Agilen Werte und das Eisbergmodell
Das **Eisbergmodell** verdeutlicht, dass unser Verhalten nur die Spitze dessen ist, was tatsächlich in uns vorgeht. Der sichtbare Teil des Eisbergs – das Verhalten und die konkreten Handlungen – wird durch tiefere, unsichtbare Ebenen gesteuert: Überzeugungen, Werte, Emotionen und innere Haltungen. Diese unsichtbare Beziehungsebene bildet die Grundlage unseres Handelns und bestimmt, wie wir auf Situationen und Menschen reagieren.

![](./../../../x_gitressourcen/eisbergmodel.jpg)

In Bezug auf die **agilen Werte** bedeutet das, dass es nicht genügt, nur äußere Praktiken und Methoden anzuwenden (wie tägliche Meetings oder kurze Iterationen). Damit agile Werte wirklich gelebt werden, muss jeder Einzelne auch die innere Haltung entwickeln, die diesen Werten entspricht. Diese Entscheidung, agile Werte wie Offenheit, Respekt, Mut und Zusammenarbeit wirklich zu verinnerlichen, ist individuell und tief in der Beziehungsebene verankert – sie ist also **von jedem Menschen selbst abhängig**.

Jemand kann zum Beispiel auf der sichtbaren Sachebene an agilen Ritualen wie Retrospektiven oder Daily Standups teilnehmen. Doch wenn seine innere Haltung die Werte hinter diesen Ritualen (wie Vertrauen und Offenheit) nicht unterstützt, bleibt das Engagement oberflächlich. Die Person wird nicht wirklich agil handeln, weil ihr Verhalten nicht von der tiefen Überzeugung geleitet wird, die agile Prinzipien erfordern.

![](./../../../x_gitressourcen/eisbergmodelundagilepyramide.jpg)

Das Eisbergmodell macht deutlich, dass nachhaltige, echte Veränderung nur dann möglich ist, wenn Menschen sich bewusst dafür entscheiden, ihre inneren Einstellungen zu hinterfragen und gegebenenfalls anzupassen. So wird es zur persönlichen Entscheidung jedes Einzelnen, die agilen Werte auf der unsichtbaren Beziehungsebene zu verankern, um sie authentisch und dauerhaft im Verhalten widerzuspiegeln.

