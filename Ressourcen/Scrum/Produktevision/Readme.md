# Product Vision

[TOC]

Mit der Definition der Product Vision schafft man Klarheit und Transparenz, so dass die richtigen/wichtigen Dinge umgesetzt werden. Sie macht klar, welche Wertschöpfung mit dem Produkt geschaffen wird und wie und warum der unternehmerische Erfolg davon abhängt. 
 
Die Product Vision muss sowohl für das Management, Stakeholder und Fachvertreter wie auch für Entwickler klar und verständlich sein. Eine Product Vision soll Unsichtbares sichtbar oder Offensichtliches explizit und transparent machen. 
 
Vorteile einer gemeinsamen Vision 
- Sie dient als Entscheidungsgrundlage während des Projekts 
- Sie minimiert Missverständnisse 
- Sie motiviert, gemeinsam in eine Richtung zu arbeiten

Es gibt viele verschiedene Vorgehensweisen und Empfehlungen. Je nach Komplexität eines Produkts können entsprechende Techniken oder Methoden angewendet werden. 

In unserem Fall verwenden wir das Product Vision Board von [Roman Pichler](https://www.romanpichler.com/).

![](./../../../x_gitressourcen/productvisionboard.png)
 
## Vorgehensweise
Im Team gemeinsam anhand des [Formulars](./10_The_Product_Vision_Board.pdf) das zukünftige Produkt analysieren, diskutieren und persistieren. 

Hier gehts zur einer [Anleitung/ Erklärung](https://www.romanpichler.com/blog/the-product-vision-board/) durch [Roman Pichler](https://www.romanpichler.com/)
 