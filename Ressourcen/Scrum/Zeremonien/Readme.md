# Zeremonien

## [Daily Scrum](./Daily%20Scrum/Readme.md)

## [Sprintplanning](./Sprintplanning/Readme.md)

## [Sprintreview](./Sprintreview/Readme.md)

## [Retrospective](./Retrospective/Readme.md)

## [Refinement](./Refinement/Readme.md)
