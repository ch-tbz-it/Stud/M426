# Daily Scrum

[TOC]

Der Daily Scrum ist eine tägliche Besprechung innerhalb des Scrum-Frameworks, die dazu dient, die Teamkommunikation zu fördern, den Fortschritt zu überwachen und Hindernisse zu identifizieren. Hier sind die Grundlagen des Daily Scrum und einige gängige Regeln:

## Was ist der Daily Scrum?

Der Daily Scrum, auch als tägliches Stand-up-Meeting bekannt, ist ein kurzes tägliches Treffen im Scrum-Framework.
Es wird von den Mitgliedern des Scrum-Teams abgehalten, was normalerweise das Entwicklungsteam, den Scrum Master und den Product Owner einschließt.
Das Meeting dauert **in der Regel 15 Minuten oder weniger** und findet jeden Tag zur gleichen Zeit statt.

### Ziele des Daily Scrum

- **Team-Synchronisation:** Die Teammitglieder berichten kurz über ihren Fortschritt, um sicherzustellen, dass alle auf dem gleichen Stand sind.
- **Planung:** Das Team plant, was es in den nächsten 24 Stunden tun wird, um das Sprintziel zu erreichen.
- **Identifizierung von Hindernissen:** Jedes Teammitglied teilt etwaige Hindernisse oder Probleme mit, die die Arbeit behindern.

![](../../../../x_gitressourcen/3DailyScrumFragen.png)

### Gängige Regeln und Empfehlungen für den Daily Scrum

- **Stehend:** Die Teilnehmer stehen, um die Meetings kurz und fokussiert zu halten.
- **Dauer:** Das Meeting dauert höchstens 15 Minuten. Dies ermutigt zur Kürze und Konzentration.
- **Täglich:** Das Daily Scrum findet jeden Arbeitstag zur gleichen Zeit statt, um Routine und Kontinuität zu schaffen.
- **Fragen:** Teammitglieder beantworten typischerweise drei Fragen: Was habe ich gestern erreicht? Was werde ich heute tun? Gibt es Hindernisse?
- **Keine Problemlösung:** Das Daily Scrum ist keine Problembehebungssitzung. Wenn Probleme identifiziert werden, sollten sie außerhalb des Meetings gelöst werden.
- **Teilnehmer:** Nur die Mitglieder des Scrum-Teams sollten sprechen. Andere Stakeholder können zuhören, sollten jedoch nicht in die Diskussion eingreifen.
- **Product Owner und Scrum Master:** Diese Rollen sind dabei, um zu hören, unterstützen aber in der Regel nicht aktiv die Planung der Teammitglieder.
- **Fokussiert bleiben:** Das Meeting sollte auf die aktuellen Sprint-Ziele konzentriert sein, um sicherzustellen, dass das Team auf dem richtigen Weg ist.

Der Daily Scrum hilft dabei, die Transparenz im Team zu erhöhen, die Zusammenarbeit zu fördern und Probleme frühzeitig zu erkennen, was zur Anpassung des Plans und zur kontinuierlichen Verbesserung führen kann. Es ist ein wichtiger Bestandteil des Scrum-Frameworks.

## Daily Scrum Struktur nach neuem Scrum Guide
Der neue Scrum Guide, der im November 2020 veröffentlicht wurde, gibt den Scrum-Teams mehr Flexibilität in Bezug auf die Struktur des Daily Scrum. In der aktuellen Version des Scrum Guides wird nicht mehr explizit darauf hingewiesen, dass die Teammitglieder die drei Standardfragen 

1. Was habe ich gestern erreicht? 
2. Was werde ich heute tun? 
3. Gibt es Hindernisse?

beantworten müssen.

Stattdessen betont der Scrum Guide, dass das Ziel des Daily Scrum darin besteht, sich auf die Arbeit zu konzentrieren, die für das Erreichen des Sprintziels erforderlich ist. Die Teammitglieder können die Struktur und den Inhalt des Meetings so gestalten, dass es am effektivsten für sie ist.

Das bedeutet, dass die Teammitglieder während des Daily Scrums weiterhin Informationen über ihren Fortschritt, ihre Pläne und etwaige Hindernisse austauschen sollten, aber sie haben die Freiheit, dies auf eine Weise zu tun, die für ihr Team am sinnvollsten ist. Dies kann dazu führen, dass einige Teams die traditionellen drei Fragen verwenden, während andere möglicherweise eine andere Struktur wählen, die besser zu ihren Bedürfnissen passt.

Die Betonung liegt weiterhin auf der Kollaboration und der Fokussierung auf die Erreichung des Sprintziels, aber die Art und Weise, wie das Team dies im Daily Scrum erreicht, kann variieren. *Es ist wichtig, dass die Teams die Effektivität ihres Daily Scrums kontinuierlich überwachen und anpassen, um sicherzustellen, dass sie ihre Ziele erreichen.*

---
Quellen:<br>
- https://www.wibas.com/blog/daily-scrum/
- [Daily Scrum.pdf](./daily_scrum.pdf)