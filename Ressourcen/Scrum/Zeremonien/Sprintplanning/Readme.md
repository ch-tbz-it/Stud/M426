# Sprintplanning

[TOC]

Die Sprint-Planung (Sprint Planning) ist ein wichtiger Teil des Scrum-Frameworks und findet am Anfang jedes Sprints statt. Während der Sprint-Planung definiert das Scrum-Team die Ziele und den Umfang des bevorstehenden Sprints und plant, wie diese Ziele erreicht werden sollen. Die Sprint-Planungssitzung wird typischerweise in zwei Teile unterteilt:

## Sprint Planning 1 (Teil 1: WAS) 
In dieser Phase geht es darum, **WAS** im Sprint erreicht werden soll. Die Teilnehmer sind das gesamte Scrum-Team, einschließlich des Product Owners und des Scrum Masters. Die wichtigsten Schritte sind:

- **Product Backlog-Review:** Das Team überprüft die Elemente im Product Backlog und kann Fragen stellen oder Unklarheiten klären.

- **Sprint-Ziel festlegen:** Das Team und der Product Owner legen gemeinsam das Sprint-Ziel fest, das beschreibt, was am Ende des Sprints erreicht werden soll. Das Sprint-Ziel sollte für das Team motivierend und für den Product Owner wertvoll sein.

- **Backlog-Elemente auswählen:** Das Team wählt die Backlog-Elemente bzw. User Stories aus dem Product Backlog aus, die für die Erreichung des Sprint-Ziels am wichtigsten sind. Diese Elemente werden dann in den Sprint-Backlog übertragen.

- **Aufwandschätzung:** Stories welche noch nicht geschätzt sind, können jetzt noch nachträglich gemacht werden ([Planning-Poker](../../Zeremonien/Refinement/Planning%20Poker/Readme.md)).

- **Kapazitätsplanung:** Das Team prüft seine Kapazität für den Sprint und stellt sicher, dass es die Arbeit erledigen kann. Wenn die Kapazität überschritten wird, müssen ggf. Aufgaben aus dem Sprint-Backlog entfernt oder reduziert werden.

## Sprint Planning 2 (Teil 2: WIE) 
In dieser Phase wird festgelegt, **WIE** die ausgewählten Backlog-Elemente im Sprint umgesetzt werden sollen. Das Entwicklungsteam arbeitet intensiv an dieser Phase, während der Product Owner und der Scrum Master als Ressourcen für Fragen zur Verfügung stehen. Die Schritte sind:

- **Sprint-Backlog erstellen:** Das Team nimmt die ausgewählten Backlog-Elemente bzw. User Stories und bricht sie in kleinere *Aufgaben (Task)* herunter, die zur Umsetzung erforderlich sind. Diese Aufgaben werden im Sprint-Backlog festgehalten.

- **Technische Diskussion:** Das Team kann technische Diskussionen führen, um sicherzustellen, dass alle Teammitglieder ein klares Verständnis davon haben, wie die Aufgaben umgesetzt werden sollen. Dies kann auch die Definition von Akzeptanzkriterien und den Ansatz zur Lösung technischer Herausforderungen einschließen.

- **Sprint-Ziel verfeinern:** Das Team kann das Sprint-Ziel weiter verfeinern, wenn es während der Diskussionen und Planung eine bessere Klarheit darüber gewonnen hat, was erreicht werden kann.

Am Ende der Sprint-Planung sollte das Scrum-Team ein klares Verständnis darüber haben, was im Sprint erreicht werden soll und wie es erreicht werden soll. Dies bildet die Grundlage für die Zusammenarbeit während des Sprints, um das Sprint-Ziel zu erreichen. Die Ergebnisse der Sprint-Planung werden im Sprint-Backlog dokumentiert, der als **Arbeitsplan** für den Sprint dient.

---
Handout: [Sprintplanning.pdf](../Sprintplanning/Sprintplanning.pdf)

Video: https://www.youtube.com/watch?v=31GETeV_fgo
