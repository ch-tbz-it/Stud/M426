# Warum relative Schätzung in SCRUM besser geeignet ist

In Scrum wird oft eine relative Schätzung verwendet, um Aufgaben und User Stories zu bewerten, anstelle von absoluten Schätzungen. Dies geschieht aus mehreren Gründen:

1. Vermeidung von Fehlgenauigkeiten: Absolute Schätzungen in Stunden oder Tagen können oft ungenau sein, insbesondere wenn es viele unbekannte Variablen gibt. Relative Schätzungen, die auf einem Vergleich der Komplexität und des Aufwands im Vergleich zu anderen Aufgaben basieren, sind tendenziell weniger fehleranfällig.

2. Einfachheit und Geschwindigkeit: Relative Schätzungen sind einfacher und schneller durchzuführen. Teammitglieder müssen nicht stundenlang über die genaue Zeit nachdenken, die für eine Aufgabe benötigt wird. Stattdessen bewerten sie einfach, wie schwer oder komplex die Aufgabe im Vergleich zu anderen Aufgaben ist.

3. Förderung des Teamzusammenhalts: Durch relative Schätzungen wird das Team in den Prozess der Aufwandschätzung einbezogen. Jedes Teammitglied bringt seine eigene Perspektive und Erfahrung mit ein, was zu einer besseren gemeinsamen Verständigung führen kann.

4. Anpassungsfähigkeit: Relative Schätzungen ermöglichen es dem Team, auf Veränderungen und Unwägbarkeiten flexibler zu reagieren. Wenn sich die Umstände ändern oder neue Informationen verfügbar werden, können Schätzungen einfacher aktualisiert werden.

5. Betonung von Wert: Scrum konzentriert sich darauf, den Wert für den Kunden zu maximieren. Relative Schätzungen sind hilfreicher, um die Priorisierung basierend auf dem Wert und der Dringlichkeit der Aufgaben zu unterstützen, da sie weniger von der genauen Zeit abhängen.

6. Psychologische Auswirkungen: Absolute Schätzungen können Druck auf Teammitglieder ausüben, ihre Schätzungen genau zu treffen. Relative Schätzungen reduzieren diesen Druck, da sie weniger präzise sein müssen.

7. Nichtlineare Arbeitsweise: In vielen Projekten ist der Aufwand für Aufgaben nicht linear. Das bedeutet, dass die Zeit, die für eine Aufgabe benötigt wird, exponentiell zunehmen kann, wenn die Komplexität steigt. Relative Schätzungen erfassen diese nichtlineare Natur oft besser.

In Scrum wird oft die Fibonacci-Folge (z. B. 1, 2, 3, 5, 8, 13, ...) oder eine ähnliche Skala verwendet, um relative Schätzungen auszudrücken. Diese Skala hilft, die Unterschiede zwischen den Aufgaben zu betonen, ohne sich auf genaue Zeitschätzungen zu konzentrieren. Auf diese Weise ermöglicht Scrum eine effiziente Planung und Priorisierung von Aufgaben und fördert die Anpassungsfähigkeit des Teams an sich ändernde Anforderungen.

---
Quellen: </br>
https://scrum.wertikalwerk.com/toolbox/agiles-schaetzen/ </br>
https://www.brainbits.net/blog/story-points-agile-punktlandung/