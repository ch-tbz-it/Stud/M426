# Planning Poker

Dieses [Handout](Planning%20Poker.pdf) hilft ihnen den Ablauf zu verstehen. Hier noch eine [ergänzende Erklärung](https://www.lise.de/blog/artikel/schaetzungen-agile-softwareentwicklung) in Textform sowie als Video. 

Auf dieser Seite können Sie Planning Poker online spielen:<br>
https://www.scrumpoker-online.org/ <br>
https://planningpokeronline.com/

