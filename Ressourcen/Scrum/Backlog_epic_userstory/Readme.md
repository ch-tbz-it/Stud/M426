# Epic / Userstory

[TOC]

## Backlog
Ein Product Backlog ist ein zentrales Konzept im Scrum-Framework und stellt eine Liste von Anforderungen, Funktionen, Verbesserungen und Ideen dar, die für ein Produkt entwickelt werden sollen. Es dient als eine Art "To-Do-Liste" für das Entwicklungsteam und wird vom Product Owner gepflegt. Der Product Backlog ist ein lebendiges Dokument, das sich im Laufe des Projekts ändern und weiterentwickeln kann.

Hier sind einige wichtige Merkmale und Aspekte des Product Backlogs in Scrum:

- **Priorisierung:** Die Elemente im Product Backlog sind nach ihrer Priorität geordnet. Der Product Owner bestimmt, welche Elemente am wichtigsten für das Produkt sind, und platziert sie oben in der Liste. Die Priorisierung basiert auf verschiedenen Faktoren, einschließlich des Kundennutzens und der Geschäftsanforderungen.

- **Anforderungsarten:** Der Product Backlog kann eine Vielzahl von Anforderungen und Aufgaben enthalten, darunter neue Funktionen, Fehlerkorrekturen, technische Schulden und Verbesserungsvorschläge. Es ist eine umfassende Liste aller Anforderungen, die für die Produktentwicklung relevant sind.

- **Schätzung:** Jedes Element im Product Backlog kann Schätzungen für den Aufwand enthalten, der erforderlich ist, um es umzusetzen. Diese Schätzungen können dem Entwicklungsteam helfen, den Zeitaufwand und die Komplexität der Arbeit abzuschätzen.

- **Änderungen und Anpassungen:** Der Product Owner kann den Product Backlog kontinuierlich aktualisieren und anpassen, um auf sich ändernde Geschäftsanforderungen, Kundenfeedback und Marktentwicklungen zu reagieren.

- **Gespräche und Klarstellungen:** Das Entwicklungsteam und der Product Owner führen regelmäßige Gespräche, um Elemente im Product Backlog zu klären, Fragen zu beantworten und sicherzustellen, dass alle Beteiligten ein gemeinsames Verständnis darüber haben, was entwickelt werden soll.

- **Sprint Planning:** Während des Sprint-Planungsprozesses wählt das Entwicklungsteam eine bestimmte Anzahl von Elementen aus dem Product Backlog aus, die in einem Sprint entwickelt werden sollen. Diese ausgewählten Elemente werden in den Sprint-Backlog übertragen.

- **Transparenz:** Der Product Backlog ist für alle im Team transparent und zugänglich. Dies ermöglicht es, dass alle Beteiligten den Fortschritt der Produktentwicklung verfolgen und verstehen können.

>Der Product Backlog ist ein dynamisches Dokument, das sich im Laufe der Zeit ändert, während das Produkt wächst und sich weiterentwickelt. Die Priorisierung und Pflege des Product Backlogs ist eine wichtige Aufgabe des Product Owners, um sicherzustellen, dass das Team stets an den wertvollsten und relevantesten Aufgaben arbeitet.

## Epic und Userstory
In Scrum sind "Epic" und "User Story" zwei Begriffe, die zur Organisation und Verwaltung von Anforderungen für die Entwicklung eines Produkts verwendet werden. Sie sind Teil des Product Backlogs und dienen dazu, die Funktionalität und die Arbeit des Entwicklungsteams zu beschreiben. 

![](../../../x_gitressourcen/productvision_epic_userstory.jpg)

Hier sind die Definitionen und Unterschiede zwischen Epic und User Story:

**Epic**

- Ein Epic ist eine große, umfassende Anforderung oder Aufgabe, die zu groß ist, um in einem einzelnen - Entwicklungszyklus oder Sprint abgeschlossen zu werden.
- Epics sind in der Regel hochrangige, strategische Anforderungen, die mehrere Funktionen oder Teilaufgaben umfassen können.
- Sie bieten einen Rahmen oder eine Übersicht über größere Initiativen oder Projektbereiche und dienen dazu, den Fokus auf die strategischen Ziele des Produkts zu richten.
- Epics können in kleinere, handlichere Aufgaben aufgeteilt werden, die als User Stories bezeichnet werden.

Beispiel für ein Epic: "Implementierung der Benutzerauthentifizierung und Autorisierung."

**User Story**

- Eine User Story ist eine kleinere, granularere Anforderung, die eine spezifische Funktionalität oder einen spezifischen Benutzerfall beschreibt.
- User Stories sind in der Regel kurz und prägnant formuliert und konzentrieren sich auf den Wert, den sie für den Benutzer oder das Unternehmen bieten.
- Sie folgen einem bestimmten Format, wie z. B. "Als [Benutzerrolle] möchte ich [Funktion], damit [Begründung/Ziel]."
- User Stories sind normalerweise klein genug, um in einem Sprint abgeschlossen zu werden und bieten eine klare Definition dessen, was erreicht werden soll.

Beispiel für eine User Story: "Als registrierter Benutzer möchte ich mich mit meiner E-Mail-Adresse und meinem Passwort anmelden, um auf mein Benutzerkonto zugreifen zu können."

## INVEST
Eine gute User Story sollte die folgenden Kriterien erfüllen, die als INVEST-Prinzip bekannt sind:

**I** - Independent (Unabhängig): User Stories sollten voneinander unabhängig sein und in beliebiger Reihenfolge entwickelt werden können.

**N** - Negotiable (Verhandelbar): User Stories sollten flexibel und verhandelbar sein, um Änderungen und Anpassungen zu ermöglichen.

**V** - Valuable (Wertvoll): User Stories sollten einen klaren Wert für den Benutzer oder das Unternehmen bieten.

**E** - Estimable (Schätzbar): User Stories sollten schätzbar sein, um den Aufwand und die Komplexität der Umsetzung abzuschätzen.

**S** - Small (Klein): User Stories sollten klein genug sein, um in einem Sprint abgeschlossen zu werden.

**T** - Testable (Testbar): User Stories sollten testbar sein, um sicherzustellen, dass die Anforderungen erfüllt sind und die Funktionalität korrekt implementiert wurde.


*Zusammenfassung*<br>
In der Praxis werden Epics oft zuerst im Product Backlog erstellt, um größere Ziele zu identifizieren. Wenn es an der Zeit ist, diese Ziele zu realisieren, werden die Epics in kleinere, umsetzbare User Stories (WAS) aufgeteilt. Dieser schrittweise Ansatz ermöglicht es dem Entwicklungsteam, die Arbeit in handliche Teile aufzuteilen und die Entwicklung in Sprints zu planen. Im Sprint Backlog leiten sich Tasks (WIE) von User Stories ab. Diese beschreiben die technische Aufgabe zur Umsetzung einer User Story.
User Stories bieten einen detaillierten Blick auf die Anforderungen aus der Benutzersicht und sind die Grundlage für die Entwicklung und das Testen im agilen Scrum-Modell.


## Quellen
- [Epic und Userstory](./epic_userstory.pdf)
- [Userstory splitten](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/)
- [Productvision, Epic und Userstories](https://www.gotscharek-company.com/pmbok-5th-ed-en-de/138-input-output)