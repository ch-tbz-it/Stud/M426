# Vortragsbewertungs-Raster

**Lernende:**________________________________
**Lehrperson:**______________________________

**Thema:**___________________________________
**Dauer:**___________________________________

**Datum:**__________________________________


---

| **Kategorie**            | **Aspekt**                       | **Bewertungsskala (1-5)**                                            | **Bemerkungen** |
|---------------------------|-----------------------------------|----------------------------------------------------------------------|-----------------|
| **Inhalt**               | Klarheit der Botschaft           | 1: Verwirrend, Unklar; 2: Unstrukturiert, Teilweise verständlich; 3: Verständlich, Logisch; 4: Präzise, Klar; 5: Glasklar, Punktgenau |                 |
|                           | Tiefe des Inhalts                | 1: Oberflächlich, Unvollständig; 2: Grundlegend, Unausgereift; 3: Angemessen, Informativ; 4: Detailliert, Fundiert; 5: Tiefgehend, Expertenhaft |                 |
|                           | Sachwissen                   | 1: Falsch, Fehlerhaft; 2: Oberflächlich, Unpräzise; 3: Korrekt, Basiswissen; 4: Fundiert, Gut recherchiert; 5: Expertenhaft, Umfassend |                 |
|                           | Struktur und Aufbau              | 1: Chaotisch, Verwirrend; 2: Ungeordnet, Sprunghaft; 3: Nachvollziehbar, Verständlich; 4: Geordnet, Fließend; 5: Perfekt strukturiert, Elegant |                 |
| **Präsentationstechnik** | Körpersprache                    | 1: Starr, Unsicher; 2: Zurückhaltend, Gehemmt; 3: Natürlich, Locker; 4: Ausdrucksvoll, Selbstbewusst; 5: Dynamisch, Inspirierend |                 |
|                           | Stimmmodulation                  | 1: Monoton, Flach; 2: Eintönig, Variationsarm; 3: Variabel, Angemessen; 4: Lebhaft, Engagiert; 5: Fesselnd, Emotional |                 |
|                           | Umgang mit Medien                | 1: Störend, Unangemessen; 2: Unsicher, Fehlerhaft; 3: Angemessen, Zweckmäßig; 4: Sicher, Effektiv; 5: Meisterhaft, Kreativ |                 |
|                           | Blickkontakt                 | 1: Vermeidend, Unsicher; 2: Unregelmäßig, Gehemmt; 3: Ausreichend, Freundlich; 4: Direkt, Zugewandt; 5: Verbindend, Vertrauensvoll |                 |
|                           | Redefluss                    | 1: Stockend, Unterbrochen; 2: Holprig, Unrhythmisch; 3: Fließend, Akzeptabel; 4: Flüssig, Reibungslos; 5: Geschmeidig, Perfekt abgestimmt |                 |
|                           | Sprechweise                  | 1: Vollständig abgelesen, Monoton, Hölzern; 2: Teilweise abgelesen, Unsicher, Eintönig; 3: Überwiegend frei, Deutlich, Akzeptabel; 4: Weitestgehend frei, Klar, Selbstbewusst; 5: Komplett frei gesprochen, Inspirierend, Natürlich |                 |
| **Interaktion**          | Kontakt mit dem Publikum         | 1: Abwesend, Ignorierend; 2: Reserviert, Zurückhaltend; 3: Zugewandt, Freundlich; 4: Engagiert, Aktiv; 5: Inspirierend, Begeisternd |                 |
|                           | Umgang mit Fragen                | 1: Ausweichend, Unsicher; 2: Zögerlich, Teilweise klar; 3: Akzeptabel, Verständlich; 4: Kompetent, Lösungsorientiert; 5: Professionell, Präzise |                 |
| **Kreativität und Wirkung** | Innovationsgrad des Vortrags    | 1: Altmodisch, Unoriginell; 2: Gewöhnlich, Vorhersehbar; 3: Interessant, Solide; 4: Kreativ, Ansprechend; 5: Bahnbrechend, Inspirierend |                 |
|                           | Emotionale Wirkung               | 1: Langweilig, Trocken; 2: Zäh, Wenig einladend; 3: Unterhaltsam, Angenehm; 4: Mitreißend, Fesselnd; 5: Begeisternd, Bewegend |                 |
| **Zeitmanagement**       | Einhaltung der Zeitvorgabe       | 1: Stark überzogen, Chaotisch; 2: Überzogen, Unausgeglichen; 3: Akzeptabel, Im Rahmen; 4: Punktgenau, Gut organisiert; 5: Vorbildlich, Perfekt abgestimmt |                 |
|                           | Tempo des Vortrags               | 1: Zu langsam, Langatmig; 2: Zäh, Schwer verständlich; 3: Angemessen, Akzeptabel; 4: Dynamisch, Fließend; 5: Perfekt ausbalanciert, Ideales Tempo |                 |

---

**Total Punkte:**_________________________

**Note:**__________________________________