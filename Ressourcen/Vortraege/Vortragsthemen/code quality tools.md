# code quality tools


[TOC]

## Inhalt

Zeitvorgabe: 7 Min.

Was macht dieses Tool und wofür ist es nützlich?
- Zählen Sie zwei bis drei häufigst verwendeten oder bekanntesten Tools auf.
- Welche Funktionen und Aufgauben werden vom Tool durchgeführt. Zählen Sie einige auf.
- Wo und zu welchem Zeitpunkt wird dieses Tool verwendet?
- Ergänzend zu Ihrer Erklärung wäre evtl. eine Demo eine gute Ergänzung.



---
