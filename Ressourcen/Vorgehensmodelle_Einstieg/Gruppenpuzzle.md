
# Gruppenpuzzle (Jigsaw-Methode)
Die [Jigsaw-Methode](https://teachingtools.uzh.ch/en/tools/gruppenpuzzle) ist eine kooperative Lernmethode, bei der die Lernenden in Expertengruppen arbeiten, um sich auf ein Thema zu spezialisieren. Anschließend werden die Experten in gemischte Gruppen aufgeteilt, um ihr Wissen zu teilen. Die Methode fördert die Zusammenarbeit und das Verständnis für die verschiedenen Perspektiven der Gruppenmitglieder.

| Experten Gruppe 1  | Experten Gruppe 2  | Experten Gruppe 3  | Experten Gruppe 4  |
|-----------|-----------|-----------|-----------|
| Person 1  | Person 5  | Person 9  | Person 12 |
| Person 2  | Person 6  | Person 10 | Person 13 |
| Person 3  | Person 7  | Person 11 | Person 14 |
| Person 4  | Person 8  |           |           |


**Ablauf:** 
- Aufteilung in 3 Puzzle-Gruppen, vetreten durch die Experten Gruppen 1, 2, 3 und 4
- Thema Gruppe 1 fängt an zu erklären, danach Gruppe 2, 3 und 4
- Jede Puzzle Gruppe hat 5 Minuten Zeit, um ihr Thema zu erklären
- Kurze Feedbackrunde nach jedem Puzzle
- Abschluss Feedbackrunde am Schluss


| **Puzzle-Gruppe** | **Person aus Experten Gruppe 1** | **Person aus Experten Gruppe 2** | **Person aus Experten Gruppe 3** | **Person aus Experten Gruppe 4** | 
|--------------------|-------------------------|-------------------------|-------------------------|-------------------------|
| Puzzle 1 | Person 1, 4 | Person 5    | Person 9  | Person 12 | 
| Puzzle 2 | Person 2    | Person 6, 8 | Person 10 | Person 14 | 
| Puzzle 3 | Person 3    | Person 7    | Person 11 | Person 13 | 