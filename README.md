![TBZ Logo](x_gitressourcen/tbz_logo.png)
![scrum](x_gitressourcen/scrum.png)

---

[TOC]

# m426 - Software mit agilen Methoden entwickeln

2.Lehrjahr: Q1/Q2 für AP, Q1/(Q3) für WUP

## Modulbeschreibung

[Modulidentifikation m426](M426%20Software%20mit%20agilen%20Methoden%20entwickeln.pdf)

## LBV (Vorgaben für die Leistungsbeurteilung)
1. Vortrag
   - [Vortragsbewertungsraster](Ressourcen/Vortraege/Vortragsbewertungraster.pdf)
2. Basistest
     - Lernziele
         - Vorgehensmodelle unterscheiden und aufzählen können
   	    - Was sind lineare/ nicht-lineare (iterative) Vorgehensmodelle	
         - SCRUM: 3-5-3
           - Aufgaben der Rollen?		
           - Was geschieht in den Zeremonien?		
           - Was sind Artefakte?		
         - https://scrumguides.org/
3. Scrum-Experiment mit SCRUM-Teams
   - [Beurteilungsraster](Ressourcen/Scrum_Experiment/LB_M426_V1.4_Beurteilungsraster.pdf)
   - [Kriterien Päsentation SCRUM Experiment](Ressourcen/Scrum_Experiment/Präsentation_SCRUM_Experiment.pdf)

---
&copy;TBZ | 21.02.2023  