## Tag 06
## Programm

1. Vorträge

2. Inputs: 
   - [Agile Manifesto](https://agilemanifesto.org/)
   - [Agile Werte und Prinzipien](../Ressourcen/Scrum/Werte_und_Prinzipien/Readme.md)


---
## Hands-on

**Ende Sprint 1**
1. Review Sprint 1
2. Retrospective Sprint 1

**Sprint 2, Woche 1**

3. Start Sprint 1
   - Daily Scrum
   - Planning Sprint 2
   - Bearbeitung Sprint 2

---

&copy;TBZ | 15.03.2023