# Lektionenplanung

| Tag       | Sprint | Sprintwoche | Bemerkungen | Details         |
|-----------|--------|-------------|-------------|-----------------|
| 01    | -  | - |- Vorstellung Klasse und Lehrperson <br> - Administratives <br> - Vortragsthemen und Termine <br> - Vorgehensmodelle <br> - SCRUM|[Details Tag 01](./Tag%2001.md) |
| 02  | 0 | 1 | - Recap <br> - Vorträge <br> - Input: Product Vision Board | [Details Tag 02](./Tag%2002.md)|
| 03  | 0 | 2 | - Recap <br> - Vorträge <br> - Input: Epic/ Userstory <br> - Input: Was passiert in den Meetings (Daily Scrum, Refinement)? | [Details Tag 03](./Tag%2003.md) |
| 04| 1 | 1 | - Recap <br> - Vorträge <br> - Input: Planning Poker <br> - Input: Definition of Done <br> - Input: Sprintplanning | [Details Tag 04](./Tag%2004.md) |
| 05   | 1 | 2 | - Recap <br> - Vorträge <br> - **Basistest (ecolm.com)** <br> - Input: Review <br> - Input: Retrospective <br> - Input Burn down-Chart | [Details Tag 05](./Tag%2005.md) |
| 06    | 2 | 1 | - Recap <br> - Vorträge <br> - Input: Agile Manifesto <br> - Input: Agile Werte und Prinzipien | [Details Tag 06](./Tag%2006.md) |
| 07  | 2 | 2 | - Recap <br> - Vorträge | [Details Tag 07](./Tag%2007.md) |
| 08  | 3 | 1 | - Recap <br> - Vorträge | [Details Tag 08](./Tag%2008.md) |
| 09  | 3 | 2 |             | [Details Tag 09](./Tag%2009.md) |
| 10  | - | - | - Klassen-Review <br> - Scrum-Team-Gespräche <br> - Abschluss | [Details Tag 10](./Tag%2010.md) |

---

![](./../x_gitressourcen/M426_SCRUM_FLOW.png)
