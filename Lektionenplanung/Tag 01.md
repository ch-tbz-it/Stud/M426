# Tag 01

## Programm

- Vorstellung Klasse
  - Emotional Checkin
  - Aufstellung "agile know-how"
- Vorstellung LP
- Verhaltensregeln im (Fern-) Unterricht
- Quartalsplan/ Lektionenplanung
- MS Teams -> remote & share tool
- [LBV (Notengebung)](./../README.md#lbv-vorgaben-für-die-leistungsbeurteilung)

1. Vortragsthemen und Termine, siehe MS Teams
   - [Tipps für Vortraege](/Ressourcen/Vortraege/Tipps)
   - [Vortragsbewertungsraster](/Ressourcen/Vortraege/Vortragsbewertungraster.pdf)
2. Vorgehensmodelle
   - Input Lehrperson
   - Gruppenarbeit
   - [Informations-Ressourcen Vorgehensmodelle](/Ressourcen/Vorgehensmodelle_Einstieg/)
3. SCRUM
   - Video: [Einführung SCRUM](https://www.youtube.com/watch?v=x5mKltAb2-8)
   - [scrumguides.org](https://scrumguides.org/)
   - [Poster Scrumguide](/Ressourcen/Scrum/Scrum%20flow/Poster/Scrum%20Guide%20Poster%20de%202.0.pdf)
   - Diskussion
---
## Hands-on
1. Gruppenarbeit Vorgehensmodelle<br>
**Kickoff: SCRUM “Experiment**
2. Team-Bildung: SCRUM-Team (Optimal 4-5er Team)
3. Projektwahl/ Projektbestimmung
4. Bestimmung Scrum Master & Product Owner

---

&copy;TBZ | 21.02.2023 