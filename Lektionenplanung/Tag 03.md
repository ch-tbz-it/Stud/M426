# Tag 03

## Programm
1. Vorträge
2. Input: [Epic/ Userstory](../Ressourcen/Scrum/Backlog_epic_userstory/Readme.md#epic-und-userstory)
3. Input: Was passiert in den Meetings?
   - [Daily Scrum](../Ressourcen/Scrum/Zeremonien/Daily%20Scrum/Readme.md)
   - [Refinement: Product Backlog verfeinern](../Ressourcen/Scrum/Zeremonien/Refinement/Readme.md)

---
## Hands-on
**Sprint 0, Woche 2**
1. Reflektieren Sie nochmals den [SCRUM-Flow](https://www.youtube.com/watch?v=A26apfhIWEE)
2. Versuchen Sie ihre erste Zeremonie gem. Handout: [Daily Scrum](/Ressourcen/Scrum/Zeremonien/daily_scrum.pdf) umzusetzen
   - Zusätzliche Hilfe: [Video Erklärung](https://www.youtube.com/watch?v=PbPmYbMzERg)
3. Erstellen Sie ihr [Product Backlog](../Ressourcen/Scrum/Backlog_epic_userstory/Readme.md#backlog), u.a. sollen Sie Userstories entwickeln. Hilfestellungen finden Sie hier:
   - [Techniken, um Userstories zu zerlegen](https://www.dasscrumteam.com/de/user-stories)
     - [Video1 (User Stories)](https://vimeo.com/405408141)
     - [Video2 (Epics)](https://vimeo.com/405408146/562091d462)
     - [Video3 (Aspekte)](https://vimeo.com/405408155/7010173449)
     - [Poster Scrum Kompakt](/Ressourcen/Scrum/Scrum%20flow/Poster/User%20Story%20kompakt%20de.pdf)
   
   Eine gute Vorgehensweise, wie Sie Userstories identifizieren, finden Sie [hier](/Ressourcen/Scrum/Backlog_epic_userstory/Product%20vision%20to%20storymap.rtb). Importieren Sie das Miroboard und füllen Sie es aus.

4. Priorisieren Sie ihr Product Backlog mit einer der Methoden ([Uebersicht Methoden](https://agilesverwaltungswissen.org/wiki/Kategorie:Priorisierungsmethoden))
   - [MoSCoW-Priorisierung](https://agilesverwaltungswissen.org/wiki/MoSCoW-Priorisierung)
   - [Eisenhower Matrix](https://agilesverwaltungswissen.org/wiki/Eisenhower-Matrix)
   - [Nutzen-orientierte Priorisierung](https://agilesverwaltungswissen.org/wiki/Nutzen-orientierte_Priorisierung)
  

---

&copy;TBZ | 21.02.2023