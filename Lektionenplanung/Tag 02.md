# Tag 02
- Ergänzung Vorgehensmodelle: linear/ iterativ
  - [Einordnung lineare/ iterative Vorgehensmodelle](https://www.scnsoft.de/blog/vorgehensmodelle-der-softwareentwicklung)

## Programm
1. Recap: Was ist Scrum? 
   - Gruppenarbeit SCRUM-Team: Erklären Sie den scrum flow, indem Sie die Bilder am richtigen Ort platzieren. 
   - [Timeboxed](https://timer.yodi.io/): 15 Min. 
   - Q&A im Plenum
2. Vorträge
3. Input: «Product Vision»/ [Product Vision Board Process](/Ressourcen/Scrum/Produktevision/Product_vision_board_process.pdf)

---
## Hands-on
**Sprint 0, Woche 1**
1. Check Scrum-Teams
   - Team Zusammensetzungen
   - Projekte
2. Auftrag «Product Vision»
   - Erarbeitung «Product Vision» im SCRUM-Team. Dieses [miro-template](/Ressourcen/Scrum/Backlog_epic_userstory/Product%20vision%20to%20storymap.rtb) könnt ihr in [miro.com](https://miro.com/) importieren und ausfüllen.
     - [Hier](./../Ressourcen/Scrum/Produktevision/Readme.md) könnt ihr nochmals nachlesen über Product Vision.
   - Timeboxed: 45 Min.
   - Optional: [Elevator pitch](https://karrierebibel.de/elevator-pitch/)

---

&copy;TBZ | 21.02.2023 
